import { configureStore } from "@reduxjs/toolkit";
import fetchProductsSlice from "./redux/fetchProductsSlice";

const store = configureStore({
    reducer: {
        fetchP: fetchProductsSlice
    }
});

export default store;