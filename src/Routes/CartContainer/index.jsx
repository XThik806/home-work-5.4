import React from 'react';
import Cart from '../Cart';
import styles from './CartContainer.module.scss';

const CartContainer = ({ cartItems = [], removeCart = () => {} }) => {

    return (
        <div className={styles.cartContainer}>
            {/* <button onClick={console.log(cartItems)}>button</button> */}
            {cartItems.map(item => (
                <Cart key={item.id} item={item} removeCart={removeCart} />
            ))}
            <div className={styles.buttons}>
                <button className={styles.clear}>Clear cart</button>
                <button className={styles.pay}>Proceed payment</button>
            </div>
        </div>
    );
}

export default CartContainer;
